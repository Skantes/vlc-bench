package org.videolan.vlcbenchmark.tools.vlc

import org.junit.Assert
import org.junit.Test

class VLCVersionTests {

    @Test
    @Throws(Exception::class)
    fun testVersionMatchRegex() {
        Assert.assertEquals(
            "3.5.4".matches(Regex("([0-9]+).([0-9]+).([0-9]+)")),
            true)
        Assert.assertEquals(
            "10.10.10".matches(Regex("([0-9]+).([0-9]+).([0-9]+)")),
            true)
        Assert.assertEquals(
            "3.5.b".matches(Regex("([0-9]+).([0-9]+).([0-9]+)")),
            false)
        Assert.assertEquals(
            "unknown".matches(Regex("([0-9]+).([0-9]+).([0-9]+)")),
            false)
    }
    @Test
    @Throws(Exception::class)
    fun testCompareTo() {
        Assert.assertEquals(0,
            VLCVersion("3.5.4")
                .compareTo(VLCVersion("3.5.4"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4")
                .compareTo(VLCVersion("3.5.3"))
        )
        Assert.assertEquals(-1,
            VLCVersion("3.5.3")
                .compareTo(VLCVersion("3.5.4"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4")
                .compareTo(VLCVersion("3.4.1"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4")
                .compareTo(VLCVersion("3.5.4 Beta"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4 RC")
                .compareTo(VLCVersion("3.5.4 Beta"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4 RC")
                .compareTo(VLCVersion("3.5.4 Alpha"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4 RC")
                .compareTo(VLCVersion("3.5.4 Dev"))
        )
        Assert.assertEquals(-1,
            VLCVersion("3.5.4 RC")
                .compareTo(VLCVersion("3.5.4 unknown"))
        )
        Assert.assertEquals(-1,
            VLCVersion("3.5.4 Beta")
                .compareTo(VLCVersion("3.5.4 RC"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4 Beta")
                .compareTo(VLCVersion("3.5.4 Alpha"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4 Beta")
                .compareTo(VLCVersion("3.5.4 Dev"))
        )
        Assert.assertEquals(-1,
            VLCVersion("3.5.4 Alpha")
                .compareTo(VLCVersion("3.5.4 RC"))
        )
        Assert.assertEquals(-1,
            VLCVersion("3.5.4 Alpha")
                .compareTo(VLCVersion("3.5.4 Beta"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4 Alpha")
                .compareTo(VLCVersion("3.5.4 Dev"))
        )
        Assert.assertEquals(-1,
            VLCVersion("3.5.4")
                .compareTo(VLCVersion("benchmark"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4 Beta 3")
                .compareTo(VLCVersion("3.5.4 Beta 2"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4 RC 1")
                .compareTo(VLCVersion("3.5.4 Beta 2"))
        )
        Assert.assertEquals(1,
            VLCVersion("3.5.4")
                .compareTo(VLCVersion("3.5.4 RC 1"))
        )
        Assert.assertEquals(1,
            VLCVersion("4.0.0")
                .compareTo(VLCVersion("3.5.4 Beta 2"))
        )
        Assert.assertEquals(-1,
            VLCVersion("4.0.0")
                .compareTo(VLCVersion("3.5.4 Beta BadNumberFormat"))
        )
        Assert.assertEquals(1,
            VLCVersion("4.0.0")
                .compareTo(VLCVersion("3.5.4"))
        )
        Assert.assertEquals(1,
            VLCVersion("4.0.0-preview - 3050400")
                .compareTo(VLCVersion("3.5.4"))
        )
    }
}