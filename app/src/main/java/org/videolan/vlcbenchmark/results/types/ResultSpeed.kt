/*
 *****************************************************************************
 * ResultSpeed.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.results.types

import kotlinx.android.parcel.Parcelize
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.results.ResultModel

@Parcelize
class ResultSpeed(
        override var name: String = "",
        override var hardware: Boolean = false,
        override var score: Double = 50.0,
        override var maxScore: Double = 50.0,
        override var type: Constants.ResultType = Constants.ResultType.SPEED,
        override var crash: String = "",
        override var stacktrace: String = "",
        var speed: Double = 1.0
) : ResultModel(name, hardware, score, maxScore, type, crash, stacktrace) {
    constructor(jsonObject: JSONObject) : this() {
        this.name = jsonObject.getString("name")
        this.score = jsonObject.getInt("score").toDouble()
        this.hardware = jsonObject.getBoolean("hardware")
        this.crash = jsonObject.getString("crash")
        this.stacktrace = jsonObject.getString("stacktrace")
        this.speed = jsonObject.getDouble("speed")
    }

    override fun jsonDump(): JSONObject {
        val jsonObject = super.jsonDump()
        jsonObject.put("speed", this.speed)
        jsonObject.put("max_score", this.maxScore)
        return jsonObject
    }

    fun setResults(speed: Double) {
        if (speed < 1.0)
            this.score *= speed
        this.speed = speed
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }
}