package org.videolan.vlcbenchmark.results

import android.animation.Animator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.result_sample_row.view.*
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.results.types.ResultPlayback
import org.videolan.vlcbenchmark.results.types.ResultQuality
import org.videolan.vlcbenchmark.results.types.ResultSpeed
import org.videolan.vlcbenchmark.tools.FormatStr
import org.videolan.vlcbenchmark.tools.Util

class ResultSampleAdapter(val results: ResultSampleList, val openedList: ArrayList<Int>) : RecyclerView.Adapter<ResultSampleAdapter.ViewHolder>() {
    
    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.result_sample_row, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var isExpanded = openedList.contains(position)
        holder.itemView.isActivated = isExpanded
        if (isExpanded) {
            holder.itemView.result_layout.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.grey100))
            holder.itemView.result_handle.rotation = 90f
            holder.itemView.result_collapsible.visibility = View.VISIBLE
        } else {
            holder.itemView.result_layout.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.pureWhite))
            holder.itemView.result_handle.rotation = 0f
            holder.itemView.result_collapsible.visibility = View.GONE
        }
        holder.itemView.setOnFocusChangeListener { v, hasFocus ->
            if (Util.isAndroidTV(v.context)) {
                if (hasFocus) {
                    v.result_layout.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.grey200))
                } else {
                    if (isExpanded)
                        v.result_layout.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.grey100))
                    else
                        v.result_layout.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.pureWhite))
                }
            }
        }
        val clickView = if (Util.isAndroidTV(holder.itemView.context))
            holder.itemView
        else
            holder.itemView.result_layout
        clickView.setOnClickListener {
            if (isExpanded) {
                openedList.remove(position)
                collapse(holder)
                isExpanded = false
            } else {
                openedList.add(position)
                expand(holder)
                isExpanded = true
            }
        }
        holder.setDataResults(results.results[position])
    }

    private fun expand(holder: ViewHolder) {
        val view = holder.itemView.result_collapsible
        val handle = holder.itemView.result_handle
        handle.animate().rotation(90f).setListener(null)
        view.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val measuredHeight = view.measuredHeight
        view.layoutParams.height = 0
        view.visibility = View.VISIBLE
        val animation: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                view.layoutParams.height =
                    if (interpolatedTime == 1f) ViewGroup.LayoutParams.WRAP_CONTENT
                    else (measuredHeight * interpolatedTime).toInt()
                view.requestLayout()
            }
        }
        animation.duration = 300
        holder.itemView.result_layout.setBackgroundColor(ContextCompat.getColor(view.context, R.color.grey100))
        view.startAnimation(animation)
    }

    private fun collapse(holder: ViewHolder) {
        val view = holder.itemView.result_collapsible
        val handle = holder.itemView.result_handle
        handle.animate().rotation(0f).setListener(object : Animator.AnimatorListener {
            override fun onAnimationEnd(animation: Animator?) {
                holder.itemView.result_layout.setBackgroundColor(ContextCompat.getColor(view.context, R.color.pureWhite))
            }
            override fun onAnimationRepeat(animation: Animator?) {}
            override fun onAnimationCancel(animation: Animator?) {}
            override fun onAnimationStart(animation: Animator?) {}
        })
        val measuredHeight = view.measuredHeight
        val animation: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                if (interpolatedTime == 1f) {
                    view.visibility = View.GONE
                } else {
                    view.layoutParams.height =
                        measuredHeight - (measuredHeight * interpolatedTime).toInt()
                    view.requestLayout()
                }
            }
        }
        animation.duration = 300
        view.startAnimation(animation)
    }

    override fun getItemCount(): Int {
        return results.results.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        lateinit var result: ResultSample
        private var hardwareScore: Double = 0.0
        private var softwareScore: Double = 0.0
        private var hardwareMaxScore: Double = 0.0
        private var softwareMaxScore: Double = 0.0

        private val TAG = this::class.java.name

        private fun setSoftwareTitleVisible() {
            itemView.software_title.visibility = View.VISIBLE
            itemView.software_score.visibility = View.VISIBLE
        }

        private fun setHardwareTitleVisible() {
            itemView.hardware_title.visibility = View.VISIBLE
            itemView.hardware_score.visibility = View.VISIBLE
        }

        private fun resetVisibility() {
            itemView.software_title.visibility = View.GONE
            itemView.software_score.visibility = View.GONE

            itemView.software_title_playback.visibility = View.GONE
            itemView.software_playback_score.visibility = View.GONE
            itemView.software_playback_frames_dropped.visibility = View.GONE
            itemView.software_playback_frames_dropped_score.visibility = View.GONE
            itemView.software_playback_warnings.visibility = View.GONE
            itemView.software_playback_warnings_score.visibility = View.GONE
            itemView.software_playback_crash.visibility = View.GONE

            itemView.software_title_quality.visibility = View.GONE
            itemView.software_quality_score.visibility = View.GONE
            itemView.software_quality_bad_screenshots.visibility = View.GONE
            itemView.software_quality_bad_screenshots_score.visibility = View.GONE
            itemView.software_quality_crash.visibility = View.GONE

            itemView.software_title_speed.visibility = View.GONE
            itemView.software_speed_score.visibility = View.GONE
            itemView.software_speed_speed.visibility = View.GONE
            itemView.software_speed_speed_score.visibility = View.GONE
            itemView.software_speed_crash.visibility = View.GONE

            itemView.hardware_title.visibility = View.GONE
            itemView.hardware_score.visibility = View.GONE

            itemView.hardware_title_playback.visibility = View.GONE
            itemView.hardware_playback_score.visibility = View.GONE
            itemView.hardware_playback_frames_dropped.visibility = View.GONE
            itemView.hardware_playback_frames_dropped_score.visibility = View.GONE
            itemView.hardware_playback_warnings.visibility = View.GONE
            itemView.hardware_playback_warnings_score.visibility = View.GONE
            itemView.hardware_playback_crash.visibility = View.GONE

            itemView.hardware_title_quality.visibility = View.GONE
            itemView.hardware_quality_score.visibility = View.GONE
            itemView.hardware_quality_bad_screenshots.visibility = View.GONE
            itemView.hardware_quality_bad_screenshots_score.visibility = View.GONE
            itemView.hardware_quality_crash.visibility = View.GONE

            itemView.hardware_title_speed.visibility = View.GONE
            itemView.hardware_speed_score.visibility = View.GONE
            itemView.hardware_speed_speed.visibility = View.GONE
            itemView.hardware_speed_speed_score.visibility = View.GONE
            itemView.hardware_speed_crash.visibility = View.GONE
        }

        fun setDataResults(res: ResultSample) {
            result = res

            hardwareScore = 0.0
            softwareScore = 0.0
            hardwareMaxScore = 0.0
            softwareMaxScore = 0.0
            resetVisibility()

            for (r in result.results) {
                when {
                    !r.hardware && r.type == Constants.ResultType.PLAYBACK -> {
                        setSoftwareTitleVisible()

                        itemView.software_title_playback.visibility = View.VISIBLE
                        itemView.software_playback_score.visibility = View.VISIBLE
                        itemView.software_playback_frames_dropped.visibility = View.VISIBLE
                        itemView.software_playback_frames_dropped_score.visibility = View.VISIBLE
                        itemView.software_playback_percentage_frames_dropped.visibility = View.VISIBLE
                        itemView.software_playback_percentage_frames_dropped_score.visibility = View.VISIBLE
                        itemView.software_playback_warnings.visibility = View.VISIBLE
                        itemView.software_playback_warnings_score.visibility = View.VISIBLE

                        val frames = (r as ResultPlayback).framesDropped
                        val percentage = if (r.totalFrames != 0)
                            FormatStr.format2Dec(frames.toDouble() / r.totalFrames.toDouble() * 100.0)
                        else "0"
                        itemView.software_playback_frames_dropped_score.text =
                            String.format(itemView.context.getString(R.string.score), frames, r.totalFrames)
                        itemView.software_playback_percentage_frames_dropped_score.text = String.format(itemView.context.getString(R.string.decimal_percent), percentage)

                        itemView.software_playback_warnings_score.text =
                            String.format(itemView.context.getString(R.string.simple_score), r.warnings)
                        if (r.crash != "") {
                            itemView.software_playback_crash.visibility = View.VISIBLE
                            itemView.software_playback_crash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        softwareScore += r.score
                        softwareMaxScore += r.maxScore
                        itemView.software_playback_score.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                    !r.hardware && r.type == Constants.ResultType.QUALITY -> {
                        setSoftwareTitleVisible()

                        itemView.software_title_quality.visibility = View.VISIBLE
                        itemView.software_quality_score.visibility = View.VISIBLE
                        itemView.software_quality_bad_screenshots.visibility = View.VISIBLE
                        itemView.software_quality_bad_screenshots_score.visibility = View.VISIBLE

                        val screenshots = (r as ResultQuality).percentOfBadScreenshot
                        itemView.software_quality_bad_screenshots_score.text =
                            String.format(itemView.context.getString(R.string.simple_percent), screenshots.toInt())
                        if (r.crash != "") {
                            itemView.software_quality_crash.visibility = View.VISIBLE
                            itemView.software_quality_crash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        softwareScore += r.score
                        softwareMaxScore += r.maxScore
                        itemView.software_quality_score.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                    !r.hardware && r.type == Constants.ResultType.SPEED -> {
                        setSoftwareTitleVisible()

                        itemView.software_title_speed.visibility = View.VISIBLE
                        itemView.software_speed_score.visibility = View.VISIBLE
                        itemView.software_speed_speed.visibility = View.VISIBLE
                        itemView.software_speed_speed_score.visibility = View.VISIBLE

                        val speed = (r as ResultSpeed).speed
                        itemView.software_speed_speed_score.text =
                            String.format(itemView.context.getString(R.string.simple_decimal_str), FormatStr.format2Dec(speed))
                        if (r.crash != "") {
                            itemView.software_speed_crash.visibility = View.VISIBLE
                            itemView.software_speed_crash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        softwareScore += r.score
                        softwareMaxScore += r.maxScore
                        itemView.software_speed_score.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                    r.hardware && r.type == Constants.ResultType.PLAYBACK -> {
                        setHardwareTitleVisible()

                        itemView.hardware_title_playback.visibility = View.VISIBLE
                        itemView.hardware_playback_score.visibility = View.VISIBLE
                        itemView.hardware_playback_frames_dropped.visibility = View.VISIBLE
                        itemView.hardware_playback_frames_dropped_score.visibility = View.VISIBLE
                        itemView.hardware_playback_percentage_frames_dropped.visibility = View.VISIBLE
                        itemView.hardware_playback_percentage_frames_dropped_score.visibility = View.VISIBLE
                        itemView.hardware_playback_warnings.visibility = View.VISIBLE
                        itemView.hardware_playback_warnings_score.visibility = View.VISIBLE

                        val frames = (r as ResultPlayback).framesDropped
                        val percentage = if (r.totalFrames != 0)
                            FormatStr.format2Dec(frames.toDouble() / r.totalFrames.toDouble() * 100.0)
                        else "0"
                        itemView.hardware_playback_frames_dropped_score.text =
                            String.format(itemView.context.getString(R.string.score), frames, r.totalFrames)
                        itemView.hardware_playback_percentage_frames_dropped_score.text =
                            String.format(itemView.context.getString(R.string.decimal_percent), percentage)
                        itemView.hardware_playback_warnings_score.text =
                            String.format(itemView.context.getString(R.string.simple_score), r.warnings)
                        if (r.crash != "") {
                            itemView.hardware_playback_crash.visibility = View.VISIBLE
                            itemView.hardware_playback_crash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        hardwareScore += r.score
                        hardwareMaxScore += r.maxScore
                        itemView.hardware_playback_score.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                    r.hardware && r.type == Constants.ResultType.QUALITY -> {
                        setHardwareTitleVisible()

                        itemView.hardware_title_quality.visibility = View.VISIBLE
                        itemView.hardware_quality_score.visibility = View.VISIBLE
                        itemView.hardware_quality_bad_screenshots.visibility = View.VISIBLE
                        itemView.hardware_quality_bad_screenshots_score.visibility = View.VISIBLE

                        val screenshots = (r as ResultQuality).percentOfBadScreenshot
                        itemView.hardware_quality_bad_screenshots_score.text =
                            String.format(itemView.context.getString(R.string.simple_percent), screenshots.toInt())
                        if (r.crash != "") {
                            itemView.hardware_quality_crash.visibility = View.VISIBLE
                            itemView.hardware_quality_crash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        hardwareScore += r.score
                        hardwareMaxScore += r.maxScore
                        itemView.hardware_quality_score.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                    r.hardware && r.type == Constants.ResultType.SPEED -> {
                        setHardwareTitleVisible()

                        itemView.hardware_title_speed.visibility = View.VISIBLE
                        itemView.hardware_speed_score.visibility = View.VISIBLE
                        itemView.hardware_speed_speed.visibility = View.VISIBLE
                        itemView.hardware_speed_speed_score.visibility = View.VISIBLE

                        val speed = (r as ResultSpeed).speed
                        itemView.hardware_speed_speed_score.text =
                            String.format(itemView.context.getString(R.string.simple_decimal_str), FormatStr.format2Dec(speed))
                        if (r.crash != "") {
                            itemView.hardware_speed_crash.visibility = View.VISIBLE
                            itemView.hardware_speed_crash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        hardwareScore += r.score
                        hardwareMaxScore += r.maxScore
                        itemView.hardware_speed_score.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                }
            }
            itemView.software_score.text = String.format(itemView.context.getString(R.string.score), softwareScore.toInt(), softwareMaxScore.toInt())
            itemView.hardware_score.text = String.format(itemView.context.getString(R.string.score), hardwareScore.toInt(), hardwareMaxScore.toInt())
            val percent = ((softwareScore + hardwareScore) / (softwareMaxScore + hardwareMaxScore) * 100.0).toInt()
            itemView.result_score.text = String.format(itemView.context.getString(R.string.simple_percent), percent)
            itemView.result_sample_name.text = result.name
        }
    }
}