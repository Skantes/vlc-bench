package org.videolan.vlcbenchmark.results

class ResultSampleList(resultList: ArrayList<ResultModel>) {

    val results: ArrayList<ResultSample> = arrayListOf()

    init {
        mainLoop@ for (result in resultList) {
            for ((index, res) in results.withIndex()) {
                if (res.name == result.name) {
                    results[index].results.add(result)
                    continue@mainLoop
                }
            }
            val resultSample = ResultSample(result.name)
            resultSample.results.add(result)
            results.add(resultSample)
        }
    }
}