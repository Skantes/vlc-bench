/*
 *****************************************************************************
 * ResultModel.kt
 *****************************************************************************
 * Copyright © 2018-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.tools.vlc

import android.content.Context
import android.content.pm.PackageManager
import android.content.pm.Signature
import org.videolan.vlcbenchmark.BuildConfig
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.tools.vlc.VLCVersion.VersionType

/**
 * Handles all checks on VLC-Android that have to be done before starting a Benchmark:
 * checking if vlc is installed, has the right signature, and version
 */
object VLCProxy {

    /**
     * Checks if a beta version of vlc-android is required, to see if a redirect to the beta
     * program or just the store page must be displayed
     */
    fun isCurrentVLCBeta(context: Context) : Boolean {
        val actual = VLCVersion(getVLCVersion(context))
        val req = VLCVersion(BuildConfig.VLC_VERSION)
        return (req.type == VersionType.RC || req.type == VersionType.BETA)
                && (actual.type != VersionType.RC && actual.type != VersionType.BETA)
    }

    fun getVLCVersion(context: Context): String {
        return try {
            context.packageManager.getPackageInfo(
                context.getString(R.string.vlc_package_name), 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            ""
        }
    }

    fun checkVlcVersion(context: Context): Boolean {
        return try { // tmp during the VLCBenchmark alpha, using the vlc beta
            val req = VLCVersion(BuildConfig.VLC_VERSION)
            val actual = VLCVersion(getVLCVersion(context))
            actual >= req
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    /**
     * Tool method to check if VLC's signature and ours match.
     *
     * @return true if VLC's signature matches our else false
     */
    fun checkSignature(context:Context): Boolean {
        val benchPackageName = context.packageName
        val vlcSignature: Int
        val benchSignature: Int
        val sigsVlc: Array<Signature>
        val sigs: Array<Signature>


        /* Getting application signature*/
        try {
            sigs = context.packageManager.getPackageInfo(benchPackageName, PackageManager.GET_SIGNATURES).signatures
        } catch (e: PackageManager.NameNotFoundException) {
            return false
        }

        /* Checking to see if there is any signature */
        if (sigs != null && sigs.isNotEmpty())
            benchSignature = sigs[0].hashCode()
        else
            return false

        /* Getting vlc's signature */
        try {
            sigsVlc = context.packageManager.getPackageInfo(context.getString(R.string.vlc_package_name), PackageManager.GET_SIGNATURES).signatures
        } catch (e: PackageManager.NameNotFoundException) {
            return false
        }

        /* checking to see if there is are any signatures */
        if (sigsVlc != null && sigsVlc.isNotEmpty())
            vlcSignature = sigsVlc[0].hashCode()
        else
            return false

        return benchSignature == vlcSignature
    }
}