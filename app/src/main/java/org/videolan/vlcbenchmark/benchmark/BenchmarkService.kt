/*
 *****************************************************************************
 * ScreenshotService.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.benchmark

import android.annotation.TargetApi
import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.PixelFormat
import android.hardware.display.VirtualDisplay
import android.media.Image
import android.media.ImageReader
import android.media.projection.MediaProjection
import android.os.Binder
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.*
import org.videolan.vlcbenchmark.BuildConfig
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.tests.TestRepository
import org.videolan.vlcbenchmark.tests.TestRepository.getTestController
import org.videolan.vlcbenchmark.tests.TestSample
import org.videolan.vlcbenchmark.tools.FormatStr.byteRateToString
import org.videolan.vlcbenchmark.tools.FormatStr.byteSizeToString
import org.videolan.vlcbenchmark.tools.FormatStr.format2Dec
import org.videolan.vlcbenchmark.tools.StorageManager
import org.videolan.vlcbenchmark.tools.StorageManager.checkFileSum
import org.videolan.vlcbenchmark.tools.StorageManager.delete
import org.videolan.vlcbenchmark.tools.StorageManager.getInternalDirStr
import org.videolan.vlcbenchmark.tools.StorageManager.setNoMediaFile
import org.videolan.vlcbenchmark.tools.Util
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.ConnectException
import java.net.URL
import java.security.GeneralSecurityException
import java.util.*


class BenchmarkService : Service() {
    private val notificationChannelId = "VLCBENCHMARK SERVICE CHANNEL"
    private val notificationIntId = 1

    private var virtualDisplay: VirtualDisplay? = null
    private var imageReader: ImageReader? = null
    private var handler: Handler = Handler()
    private var screenshotNumber: Int = 0
    private var mediaProjection: MediaProjection? = null
    private var binder: Binder = ScreenshotServiceBinder()
    private var notificationManager: NotificationManager? = null
    private lateinit var notificationBuilder: Notification.Builder
    private var state = BenchmarkServiceState.DOWNLOAD

    private val serviceJob = Job()
    private val serviceScope = CoroutineScope(Dispatchers.IO + serviceJob)

    private var width: Int = 0
    private var height: Int = 0
    private var density: Int = 0
    private var hasSetup = false

    var percent = 0.0

    private var downloadCancelled = false
    private var isRunning = false
    private var errorStringId = 0
    private var totalFileSize = 0L

    private val br: BroadcastReceiver = ScreenshotBroadcastReceiver()

    enum class BenchmarkServiceState {
        DOWNLOAD,
        BENCHMARK
    }

    override fun onCreate() {
        val filter = IntentFilter(Constants.ACTION_TRIGGER_SCREENSHOT)
        filter.addAction(Constants.ACTION_START_DOWNLOAD)
        filter.addAction(Constants.ACTION_RESET_RUNNING)
        filter.addAction(Constants.ACTION_CANCEL_DOWNLOAD)
        registerReceiver(br, filter)
        val notification = createNotification()
        startForeground(notificationIntId, notification)
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            if (intent.action == Constants.ACTION_STOP_BENCHMARK_SERVICE) {
                downloadCancelled = true
                stopSelf()
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    inner class ScreenshotBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                when (it.action) {
                    Constants.ACTION_TRIGGER_SCREENSHOT -> {
                        if (it.hasExtra("screenshot")) {
                            screenshotNumber = intent.getIntExtra("screenshot", 0)
                            prepareScreenshot()
                        }
                    }
                    Constants.ACTION_RESET_RUNNING -> {
                        isRunning = false
                    }
                    Constants.ACTION_START_DOWNLOAD -> {
                        if (context != null && !isRunning) {
                            serviceScope.launch {
                                isRunning = true
                                val success = async { downloadFiles(context) }
                                val broadcastIntent = Intent()
                                if (!success.await() && errorStringId != 0) {
                                    broadcastIntent.action = Constants.ACTION_SERVICE_ERROR
                                    broadcastIntent.putExtra(Constants.EXTRA_SERVICE_ERROR, errorStringId)
                                } else {
                                    broadcastIntent.action = Constants.ACTION_DOWNLOAD_FINISHED
                                }
                                sendBroadcast(broadcastIntent)
                            }
                        }
                    }
                    Constants.ACTION_CANCEL_DOWNLOAD -> {
                        downloadCancelled = true
                    }
                    else -> {}
                }
            }
        }
    }

    private fun buildNotification() {
        when (state) {
            BenchmarkServiceState.DOWNLOAD -> {
                notificationBuilder
                    .setContentTitle(getString(R.string.notif_download_content_title))
                    .setSmallIcon(R.drawable.ic_icon_benchmark_white)
                    .setTicker(getString(R.string.notif_download_content_title))
                    .setProgress(100, 0, false)

            }
            BenchmarkServiceState.BENCHMARK -> {
                notificationBuilder
                    .setContentTitle(getString(R.string.notif_benchmark_content_title))
                    .setSmallIcon(R.drawable.ic_icon_benchmark_white)
                    .setContentText(getString(R.string.notif_benchmark_content_text))
                    .setTicker(getString(R.string.notif_benchmark_content_title))
            }
        }
    }

    private fun createNotification(): Notification {
        // depending on the Android API that we're dealing with we will have
        // to use a specific method to create the notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager;
            val channel = NotificationChannel(
                    notificationChannelId,
                    "VLCBenchmark notifications channel",
                    NotificationManager.IMPORTANCE_LOW
            ).let {
                it.description = getString(R.string.notif_benchmark_channel_description)
                it
            }
            notificationManager?.createNotificationChannel(channel)
        }

        val pendingIntent: PendingIntent = Intent(this, BenchmarkStepper::class.java).let { notificationIntent ->
            PendingIntent.getActivity(this, 0, notificationIntent, 0)
        }
        val stopIntent = Intent(this, StopNotificationBroadcastReceiver::class.java).apply {
            action = Constants.ACTION_STOP_BENCHMARK_SERVICE
            putExtra(Constants.EXTRA_NOTIFICATION_ID, 0)
        }
        val stopPendingIntent: PendingIntent = PendingIntent.getBroadcast(this, 0, stopIntent, 0)

        notificationBuilder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) Notification.Builder(
                this,
                notificationChannelId
        ) else Notification.Builder(this)

        notificationBuilder
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_icon_benchmark_white)
            .addAction(android.R.drawable.ic_menu_close_clear_cancel, getString(R.string.notification_btn_close), stopPendingIntent)
            .setPriority(Notification.PRIORITY_LOW) // for under android 26 compatibility
        buildNotification()
        return notificationBuilder.build()
    }

    override fun onBind(p0: Intent?): IBinder {
        return binder
    }

    override fun onDestroy() {
        downloadCancelled = true
        notificationManager?.cancel(notificationIntId)
        serviceJob.cancel()
        unregisterReceiver(br)
        super.onDestroy()
    }

    fun setupBenchmark() {
        this.state = BenchmarkServiceState.BENCHMARK
        createNotification()
        notificationManager?.notify(notificationIntId, notificationBuilder.build())

        this.width = ScreenshotInitVariables.width
        this.height = ScreenshotInitVariables.height
        this.density = ScreenshotInitVariables.dpi
        this.mediaProjection = ScreenshotInitVariables.mediaProjection
        hasSetup = true
    }

    fun prepareScreenshot() {
        if (height < 1 || width < 1) {
            setupBenchmark()
        }
        imageReader = ImageReader.newInstance(width, height, PixelFormat.RGBA_8888, 2)
        imageReader?.let {
            virtualDisplay = mediaProjection?.createVirtualDisplay(
                "testScreenshot", width,
                height, density, VIRTUAL_DISPLAY_FLAGS,
                it.surface, null, handler
            )

            if (virtualDisplay == null) {
                Log.e(TAG, "prepareScreenshot: Failed to create Virtual Display")
            }
            try {
                it.setOnImageAvailableListener(ImageAvailableListener(), handler)
            } catch (e: IllegalArgumentException) {
                Log.e(TAG, "prepareScreenshot: Failed to create screenshot callback")
            }
        }
    }

    inner class ScreenshotServiceBinder : Binder() {
        fun getService() : BenchmarkService = this@BenchmarkService
    }

    fun continueVlcAndroid() {
        val broadcastIntent = Intent(Constants.ACTION_CONTINUE_BENCHMARK)
        broadcastIntent.setPackage(getString(R.string.vlc_package_name))
        sendBroadcast(broadcastIntent)
    }

    /**
     * Callback that is called when the first image is available after setting up
     * ImageReader in onEventReceive(...) at the end of the video buffering.
     *
     * It takes the screenshot.
     */
    @TargetApi(19)
    private inner class ImageAvailableListener : ImageReader.OnImageAvailableListener {
        @TargetApi(21)
        override fun onImageAvailable(reader: ImageReader) {
            handler.postDelayed({
                var outputStream: FileOutputStream? = null
                val image: Image?
                val bitmap: Bitmap?
                try {
                    image = reader.acquireLatestImage()
                } catch (e: IllegalArgumentException) {
                    Log.e(TAG, "Failed to acquire latest image for screenshot.")
                    return@postDelayed
                }

                if (image != null) {
                    val planes = image.planes
                    val buffer = planes[0].buffer.rewind()
                    val pixelStride = planes[0].pixelStride
                    val rowStride = planes[0].rowStride
                    val rowPadding = rowStride - pixelStride * width

                    bitmap = Bitmap.createBitmap(width + rowPadding / pixelStride, height,
                            Bitmap.Config.ARGB_8888)
                    if (bitmap != null) {
                        val screenshotDir = StorageManager.getInternalDirStr(StorageManager.screenshotFolder)
                        bitmap.copyPixelsFromBuffer(buffer)
                        val folder = File(screenshotDir)

                        if (!folder.exists()) {
                            if (!folder.mkdir()) {
                                Log.e(TAG, "onImageAvailable: Failed to create screenshot directory")
                                return@postDelayed
                            }
                        }
                        val imageFile = File(folder.absolutePath + File.separator + "Screenshot_" + screenshotNumber + ".png")
                        try {
                            outputStream = FileOutputStream(imageFile)
                        } catch (e: IOException) {
                            Log.e(TAG, "Failed to create outputStream")
                        }

                        if (outputStream != null) {
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
                        }

                        bitmap.recycle()
                        image.close()
                        if (outputStream != null) {
                            try {
                                outputStream.flush()
                                outputStream.close()
                            } catch (e: IOException) {
                                Log.e(TAG, "Failed to release outputStream")
                            }

                        }
                    }
                } else {
                    Log.e(TAG, "onImageAvailable: Failed to get image")
                }
                try {
                    imageReader?.setOnImageAvailableListener(null, null)
                } catch (e: IllegalArgumentException) {
                    Log.e(TAG, "Failed to delete ImageReader callback")
                }

                virtualDisplay?.release()
                virtualDisplay = null
                imageReader?.close()

                continueVlcAndroid()

            }, 3000)
        }
    }

    // Download

    /**
     * Method responsible for handling the download of a single file
     * and checking its integrity afterward.
     *
     *
     *
     * @param file     a [File] to represent the distant local file that this method will create and fill
     * @param fileData metadata about the media.
     * @param _downloadedSize the total size that was already downloaded
     * @throws IOException if the device is not connected to WIFI or LAN or if the download failed due to an IO error.
     * @return Boolean true if there was no problem, false if the individual file download failed
     */
    @Throws(IOException::class)
    private fun downloadFile(context: Context, file: File, fileData: TestSample, _downloadedSize: Long): Boolean {
        var success = true
        var downloadedSize = _downloadedSize
        file.createNewFile()
        val fileUrl = URL(context.getString(R.string.file_location_url).toString() + fileData.url)
        var fileStream: FileOutputStream? = null
        var urlStream: InputStream? = null
        try {
            fileStream = FileOutputStream(file)
            urlStream = fileUrl.openStream()
            val buffer = ByteArray(2048)
            var read: Int
            var fromTime = System.nanoTime()
            var passedTime: Long
            var passedSize: Long = 0
            read = urlStream.read(buffer, 0, 2048)
            while (read != -1) {
                if (downloadCancelled) {
                    return true
                }
                passedTime = System.nanoTime()
                fileStream.write(buffer, 0, read)
                passedSize += read.toLong()
                /* one second counter:
                   update interface for download percent and speed */
                if (passedTime - fromTime >= 1000000000) {
                    downloadedSize += passedSize
                    publishProgress(context, downloadedSize, passedSize)
                    fromTime = System.nanoTime()
                    passedSize = 0
                }
                read = urlStream.read(buffer, 0, 2048)
            }
            if (!checkFileSum(file, fileData.checksum)) {
                delete(file)
                throw GeneralSecurityException(
                    Formatter().format(
                        "Media file '%s' is incorrect, aborting",
                        fileData.url
                    ).toString()
                )
            }
        } catch (e: Exception) {
            Log.e(TAG, "Failed to download file : $e")
            errorStringId = R.string.snack_error_file_download_fail
            success = false
        } finally {
            fileStream?.close()
            urlStream?.close()
        }
        return success
    }

    /**
     * Obtain the list of [TestSample] by calling [TestRepository.getTestController],
     * prepare the environment to download the videos and
     * then check if they are already on the device and if so if the video is valid.
     */
    private fun downloadFiles(context: Context): Boolean {
        try {
            // Add nomedia file to stop vlc medialibrary from indexing the benchmark files
            setNoMediaFile()
            totalFileSize = 0L
            val testController = getTestController(context)
            val sampleList: ArrayList<TestSample> = testController.getSampleList()
            for ((_, _, _, _, size) in sampleList) {
                totalFileSize += size.toLong()
            }
            val mediaFolderStr = getInternalDirStr(StorageManager.mediaFolder)
            if (mediaFolderStr == "") {
                Log.e(TAG, "Failed to get media directory")
                errorStringId = R.string.dialog_text_download_error
                return false
            }
            val mediaFolder = File(mediaFolderStr)
            val unusedFiles = HashSet(Arrays.asList(*mediaFolder.listFiles()))
            var downloadedSize: Long = 0
            for (fileData in sampleList) {
                if (downloadCancelled) {
                    return false
                }
                val localFile = File(mediaFolder.path + '/' + fileData.name)
                if (localFile.exists()) {
                    if (localFile.isFile && checkFileSum(localFile, fileData.checksum)) {
                        fileData.localUrl = localFile.absolutePath
                        unusedFiles.remove(localFile)
                        downloadedSize += fileData.size.toLong()
                        publishProgress(context, downloadedSize, 0L)
                        continue
                    } else if (!localFile.path.contains(".nomedia") && !BuildConfig.DEBUG) {
                        // Check not to remove the nomedia file
                        delete(localFile)
                    }
                }
                if (!downloadFile(context, localFile, fileData, downloadedSize))
                    return false
                downloadedSize += fileData.size.toLong()
                fileData.localUrl = localFile.absolutePath
            }
            publishProgress(context, 0L, -1L)
            for (toRemove in unusedFiles) {
                // Check not to remove the nomedia file
                if (!toRemove.path.contains(".nomedia") && !BuildConfig.DEBUG) {
                    delete(toRemove)
                }
            }
        } catch (e: ConnectException) {
            Log.e(TAG, e.message, e)
            errorStringId = R.string.dialog_text_error_connect
            return false
        } catch (e: IOException) {
            Log.e(TAG, e.message, e)
            errorStringId = R.string.dialog_text_error_io
            return false
        } catch (e: GeneralSecurityException) {
            Log.e(TAG, e.message, e)
            errorStringId = R.string.dialog_text_download_error
            return false
        }
        return true
    }

    private fun publishProgress(context: Context, downloadedSize: Long, downloadedSpeed: Long) {
        if (!downloadCancelled) {
            val progressString: String
            if (downloadedSpeed == -1L) {
                percent = 100.0
                progressString = getString(R.string.progress_download_complete)
            } else {
                percent = downloadedSize.toDouble() / totalFileSize * 100.0
                progressString = String.format(
                    getString(R.string.dialog_text_download_progress),
                    format2Dec(percent),
                    byteRateToString(context, downloadedSpeed),
                    byteSizeToString(context, downloadedSize),
                    byteSizeToString(context, totalFileSize)
                )
            }
            
            val broadcastIntent = Intent(Constants.ACTION_UPDATE_PROGRESS)
            broadcastIntent.putExtra(Constants.EXTRA_DOWNLOAD_PERCENT, percent)
            broadcastIntent.putExtra(Constants.EXTRA_DOWNLOAD_STRING, progressString)
            sendBroadcast(broadcastIntent)

            notificationBuilder.setContentText(progressString)
            notificationBuilder.setProgress(100, percent.toInt(), false)
            if (notificationManager != null) {
                notificationManager?.notify(notificationIntId, notificationBuilder.build())
            } else {
                startForeground(notificationIntId, notificationBuilder.build())
            }
        }
    }

    companion object {
        private const val VIRTUAL_DISPLAY_FLAGS = 0
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }
}