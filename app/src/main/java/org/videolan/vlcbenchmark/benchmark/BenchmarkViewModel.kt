/*
 *****************************************************************************
 * BenchmarkViewModel.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.benchmark

import android.app.Activity
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.media.projection.MediaProjectionManager
import android.util.Log
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.LENGTH_LONG
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.tools.SystemPropertiesProxy
import org.videolan.vlcbenchmark.api.ApiCalls
import org.videolan.vlcbenchmark.results.ResultController
import org.videolan.vlcbenchmark.results.ResultModel
import org.videolan.vlcbenchmark.results.ResultRepository
import org.videolan.vlcbenchmark.tests.TestController
import org.videolan.vlcbenchmark.tests.TestSample
import org.videolan.vlcbenchmark.tools.GoogleConnectionHandler
import org.videolan.vlcbenchmark.tools.StorageManager
import org.videolan.vlcbenchmark.tools.StorageManager.getInternalDirStr
import org.videolan.vlcbenchmark.tools.Util.errorSnackbar
import java.io.File
import java.io.IOException

class BenchmarkViewModel : ViewModel() {

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }

    lateinit var testController: TestController
    lateinit var resultController: ResultController

    var finalResults: ArrayList<ResultModel>? = null
    var resultName: String? = null
    var configVersion: String? = null

    var running: Boolean = false
    var finished: Boolean = false
    var goingToNextTest: Boolean = false
    var downloadSize: Long = 0
    var spaceMissing: Long = 0
    var projectionManager: MediaProjectionManager? = null
    lateinit var googleConnectionHandler: GoogleConnectionHandler

    // This boolean is set to false at the start of the benchmark. It means that the benchmark
    // hasn't played a dummy sample yet. This is in the case there is already media running
    // whether from VLC or a third party app like youtube. The first play might have a pip window,
    // which will disappear after first play, or in the case of background playback from vlc, it
    // will restart the media that was playing instead of the media the benchmark wants.
    // This is set to true, instead of going for the next
    // sample. The sample will after this replay normally and the benchmark will continue as normal.
    var dummySample: Boolean = false


    val state: MutableLiveData<BenchmarkActivityState> by lazy {
        MutableLiveData<BenchmarkActivityState>()
    }

    fun setState(_state: BenchmarkActivityState) {
        if (state.value != _state)
            state.value = _state
    }

    @Throws(JSONException::class)
    fun addGoogleUser(jsonObject: JSONObject): JSONObject? {
        return if (googleConnectionHandler.account != null) {
            jsonObject.put("email", googleConnectionHandler.account?.email)
            jsonObject
        } else {
            Log.e(TAG, "onActivityResult: Failed to get google email")
            null
        }
    }

    fun prepareBenchmarkUpload(context: Context, data: Intent, successListener: (success: Boolean) -> Unit) {
        val view: View = (context as Activity).findViewById(R.id.scrollview)
        finalResults?.let {
            try {
                setState(BenchmarkActivityState.UPLOAD)
                val sharedPref = PreferenceManager.getDefaultSharedPreferences((context as Activity).applicationContext)
                val screenUpload = sharedPref.getBoolean(context.getString(R.string.screenshot_upload_key), false)
                var jsonObject =
                    ResultRepository.dumpResults(context, it, data, screenUpload)
                if (jsonObject == null) {
                    errorSnackbar(view, R.string.dialog_text_err_google)
                    return
                }
                jsonObject = addGoogleUser(jsonObject)
                if (jsonObject == null) {
                    errorSnackbar(view, R.string.dialog_text_err_google)
                    return
                }
                jsonObject.put("config_version", configVersion)
                if (screenUpload) {
                    ApiCalls.uploadBenchmarkWithScreenshots(
                        context,
                        jsonObject,
                        it,
                        successListener
                    )
                } else {
                    ApiCalls.uploadBenchmark(context, jsonObject, successListener)
                }
            } catch (e: JSONException) {
                Log.e(TAG, e.toString())
                errorSnackbar(view, R.string.toast_text_error_prep_upload)
            }
        }
    }

    private fun checkDeviceFreeSpace(size: Long): Boolean {
        val freeSpace = SystemPropertiesProxy.freeSpace
        if (size > freeSpace) {
            spaceMissing = size - freeSpace
            return false
        }
        return true
    }

    suspend fun checkSamples() : Int{
        var counter = 0
        downloadSize = 0
        val filesToDownload: java.util.ArrayList<TestSample>
        try {
            val sampleList = testController.getSampleList()
            if (sampleList.size == 0) {
                val errorStringId = R.string.dialog_text_error_config
                withContext(Dispatchers.Main) {
                    setState(BenchmarkActivityState.DOWNLOAD_NOTICE)
                }
                return errorStringId
            }
            val dirStr = getInternalDirStr(StorageManager.mediaFolder)
            if (dirStr == "") {
                Log.e(TAG, "doInBackground: Failed to get media folder")
                val errorStringId = R.string.dialog_text_file_creation_failure
                withContext(Dispatchers.Main) {
                    setState(BenchmarkActivityState.DOWNLOAD_NOTICE)
                }
                return errorStringId
            }
            val dir = File(dirStr)
            val files = dir.listFiles()
            filesToDownload = java.util.ArrayList()
            for (sample in sampleList) {
                counter += 1
                Log.i(TAG, "doInBackground: checking " + sample.name)
                var presence = false
                if (files != null) {
                    for (localFile in files) {
                        if (localFile.name == sample.name) {
                            sample.localUrl = localFile.absolutePath
                            presence = true
                            break
                        }
                    }
                }
                if (!presence) {
                    Log.i(TAG, "doInBackground: " + sample.name + " file is missing")
                    filesToDownload.add(sample)
                    downloadSize += sample.size.toLong()
                }
            }
        } catch (e: IOException) {
            Log.e(TAG, e.message, e)
            return R.string.dialog_text_error_conf
        }
        if (filesToDownload.size != 0) {
            Log.i(TAG, "doInBackground: Missing " + filesToDownload.size + " files")
            withContext(Dispatchers.Main) {
                if (!checkDeviceFreeSpace(downloadSize))
                    setState(BenchmarkActivityState.NO_SPACE)
                else
                    setState(BenchmarkActivityState.DOWNLOAD_NOTICE)
            }
        } else {
            withContext(Dispatchers.Main) {
                setState(BenchmarkActivityState.CHECK)
            }
        }
        return 0
    }
}
