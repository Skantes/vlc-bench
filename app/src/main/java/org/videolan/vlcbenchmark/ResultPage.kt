/*
 *****************************************************************************
 * ResultPage.kt
 *****************************************************************************
 * Copyright © 2016-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_result_page.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.videolan.vlcbenchmark.results.*
import org.videolan.vlcbenchmark.tools.FormatStr
import org.videolan.vlcbenchmark.tools.Util.isAndroidTV
import org.videolan.vlcbenchmark.tools.SystemPropertiesProxy
import kotlin.collections.ArrayList

const val OPENED_LIST_KEY = "opened_list_key"
class ResultPage : AppCompatActivity() {
    lateinit var results: ArrayList<ResultModel>
    private var recyclerView: RecyclerView? = null
    private lateinit var openedList: ArrayList<Int>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_page)
        openedList = if (savedInstanceState != null && savedInstanceState.containsKey(OPENED_LIST_KEY))
            savedInstanceState.getIntegerArrayList(OPENED_LIST_KEY)!!
        else ArrayList()
        setupUi()
    }

    private fun setupUi() {
        if (!intent.hasExtra(Constants.EXTRA_RESULT_PAGE_NAME)) {
            Log.e(TAG, "setupUi: Failed to get name extra in intent")
            return
        }
        val benchmarkName = intent.getStringExtra(Constants.EXTRA_RESULT_PAGE_NAME) ?: return
        val toolbar = findViewById<View>(R.id.main_toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_close)
        supportActionBar?.setTitle(R.string.results_page)

        val results = ResultRepository().getResultList("$benchmarkName.txt")
        if (results.isEmpty()) {
            Log.e(TAG, "setupUi: Failed to get results from file")
            return
        }
        var softwareScore = 0
        var hardwareScore = 0
        for (result in results) {
            softwareScore += if (result.hardware) 0 else result.score.toInt()
            hardwareScore += if (result.hardware) result.score.toInt() else 0
        }

        score_card_software_score.text = String.format(getString(R.string.simple_score), softwareScore)
        score_card_hardware_score.text = String.format(getString(R.string.simple_score), hardwareScore)
        result_date.text = FormatStr.toDatePrettyPrint(benchmarkName)

        val context = this
        lifecycleScope.launch(Dispatchers.IO) {
            val properName = SystemPropertiesProxy.getProperDeviceName(context)
            withContext(Dispatchers.Main) {
                result_model.text = properName
            }
        }

        val resultSampleList = ResultSampleList(results)
        val mLayoutManager: RecyclerView.LayoutManager
        val adapter: RecyclerView.Adapter<*>
        recyclerView = findViewById<View>(R.id.test_result_list) as RecyclerView
        recyclerView?.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(this)
        recyclerView?.layoutManager = mLayoutManager
        recyclerView?.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        adapter = ResultSampleAdapter(resultSampleList, openedList)
        recyclerView?.adapter = adapter
        try {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        } catch (e: NullPointerException) {
            Log.e(TAG, e.toString())
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        recyclerView?.let {
            val viewHolder = it.findViewHolderForLayoutPosition(0)
            if (isAndroidTV(this) && viewHolder != null && !viewHolder.itemView.hasFocus() && it.hasFocus()) {
                viewHolder.itemView.requestFocus()
                return
            }
        }
        super.onBackPressed()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putIntegerArrayList(OPENED_LIST_KEY, (recyclerView?.adapter as ResultSampleAdapter).openedList)
    }

    companion object {
        private val TAG = ResultPage::class.java.name
    }
}