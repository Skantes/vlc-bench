/*
 *****************************************************************************
 * Test.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.tests

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.content.FileProvider
import kotlinx.android.parcel.Parcelize
import org.videolan.vlcbenchmark.BuildConfig
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.VLCWorkerModel
import org.videolan.vlcbenchmark.tools.StorageManager
import org.videolan.vlcbenchmark.tools.StorageManager.IOnFileCheckedListener
import java.io.File

@Parcelize
open class Test (
        open var sample: TestSample = TestSample(),
        open var type: Constants.TestType = Constants.TestType.QUALITY,
        open var hardware: Boolean = false
) : ITest {

    override fun prepareIntent(context: Context, intent: Intent, listener: VLCWorkerModel.OnIntentCreatedListener) {
        StorageManager.checkFileSumAsync(File(sample.localUrl), sample.checksum, object: IOnFileCheckedListener {
            override fun onFileChecked(valid: Boolean) {
                if (!valid) {
                    Log.e(TAG, "prepareIntent: Invalid file")
                    listener.onIntentCreated(null)
                    return
                }

                val mediaFile = File(sample.localUrl)
                val uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".benchmark.VLCBenchmarkFileProvider", mediaFile)
                intent.setPackage(context.getString(R.string.vlc_package_name))
                intent.component = ComponentName(context.getString(R.string.vlc_package_name), BENCH_ACTIVITY)
                intent.setDataAndTypeAndNormalize(uri, "video/*")
                intent.putExtra(Constants.Extras.BENCHMARK, true)
                intent.putExtra(Constants.Extras.FROM_START, true)
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                // Here hardware is reversed because it was reversed in vlc-android.
                // Should be fixed in vlc-android, then here.
                intent.putExtra(Constants.Extras.HARDWARE, !hardware)
                listener.onIntentCreated(intent)
            }
        })

    }

    override fun getPrettyTypeString(): String {
        val decoderMode = if (hardware) "Hardware" else "Software"
        return "${type.toString().toLowerCase().capitalize()} $decoderMode"
    }

    override fun getTypeString(): String {
        val decoderMode = if (hardware) "Hardware" else "Software"
        return type.toString() + "_" + decoderMode
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
        private const val BENCH_ACTIVITY = "org.videolan.vlc.gui.video.benchmark.BenchActivity"
    }
}